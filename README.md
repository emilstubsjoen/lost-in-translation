# Lost in Translation
A simple React application for sign language translation using Redux to demonstration the features. 

# Components/Features

```
└── src/components/
    └── ...
```
### Login
  Login a user using username
### Navbar
  Global navbar that can access the profile page
### Profile
  Display user profile, logout and the last 10 translation of the user
### Translation
  Main page for translateing sentences to sign language

# React Redux
## Actions
```
└── src/store/actions
    ├── loginActions.js                                
    ├── translationActions.js                                        
    └── sessionActions.js
```
 
## Middleware
```
└── src/store/middleware                
    ├── loginMiddleware.js                   
    ├── translationMiddleware.js                             
    └── sessionMiddleware.js
```

## Reducers
```
└── src/store/reducers
    ├── loginReducer.js                   
    ├── translationReducer.js                               
    └── sessionReducer.js
```



## Styling 
The application is using Bootstrap 5.x

**Source:** [https://getbootstrap.com](https://getbootstrap.com)