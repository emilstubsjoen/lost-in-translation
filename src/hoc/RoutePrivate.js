import { useSelector } from 'react-redux'
import { Redirect, Route } from 'react-router-dom'
const RoutePrivate = props => {

    const { username } = useSelector(state => state.session)

    if (!username) {
        return <Redirect to="/login" />
    } else {
        return <Route {...props} />
    }

}
export default RoutePrivate