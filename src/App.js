import "./App.css";
import {
  BrowserRouter,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { Navbar } from './components/Navbar/Navbar';
import AppContainer from './hoc/AppContainer'
import Login from './components/Login/Login'
import NotFound from './components/NotFound/NotFound'
import Translation from "./components/Translation/Translation";
import Profile from "./components/Profile/Profile";
import RoutePrivate from './hoc/RoutePrivate';

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Navbar />
        <AppContainer id="upper-area">
          <h1 className="main-header mt-5">Lost In Translation</h1>
        </AppContainer>
        <Switch>
          <Route exact path="/">
            <Redirect to="/login" />
          </Route>
          <Route path="/login" component={Login} />
          <Route path="/translation" component={ Translation } />
          <RoutePrivate path="/profile" component={Profile} />
          <Route path="*" component= { NotFound } />
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
