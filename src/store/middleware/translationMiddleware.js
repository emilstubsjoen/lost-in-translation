import { TranslationAPI } from "../../components/Translation/TranslationAPI";
import { ACTION_TRANSLATION_ATTEMPTING, ACTION_TRANSLATION_ERROR, ACTION_TRANSLATION_SUCCESS, translationSuccessAction, translationErrorAction, ACTION_TRANSLATION_CLEAR } from "../actions/translationActions";
import { sessionSetAction } from "../actions/sessionActions";

export const translationMiddleware = ({ dispatch }) => next => action => {
    
    next(action)

    if (action.type === ACTION_TRANSLATION_ATTEMPTING) {
        TranslationAPI.updateUserTranslations(action.payload)
        .then(updatedUser => {
            dispatch( translationSuccessAction(updatedUser) )
        })
        .catch (error => {
            dispatch(translationErrorAction(error))
        })
    }
    if (action.type === ACTION_TRANSLATION_SUCCESS) {
        dispatch( sessionSetAction(action.payload) )
       
    }

    if(action.type === ACTION_TRANSLATION_ERROR) {
        
    }

    if(action.type === ACTION_TRANSLATION_CLEAR) {
        TranslationAPI.clearUserTranslations(action.payload)
        .then(updatedUser => {
            dispatch( translationSuccessAction(updatedUser) )
        })
        .catch (error => {
            dispatch(translationErrorAction(error))
        })
    }
}