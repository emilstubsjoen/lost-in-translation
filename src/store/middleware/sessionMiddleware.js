import { ACTION_SESSION_CLEAR, ACTION_SESSION_INIT, ACTION_SESSION_LOGOUT, ACTION_SESSION_SET, sessionClearAction, sessionSetAction } from "../actions/sessionActions"

export const sessionMiddleware = ({ dispatch }) => next => action => {

    next(action)

    if (action.type === ACTION_SESSION_INIT){
        // Read local storage 
        const storedSession = localStorage.getItem('rlit-ss')
        // and check ig session exist
        if (!storedSession){
            return
        }
        const session = JSON.parse(storedSession)
        dispatch(sessionSetAction(session))

    }

    if(action.type === ACTION_SESSION_SET) {
        localStorage.setItem('rlit-ss', JSON.stringify(action.payload))
    }

    if (action.type === ACTION_SESSION_CLEAR) {
        localStorage.removeItem('rlit-ss')
    }

    if (action.type === ACTION_SESSION_LOGOUT) {
        dispatch(sessionClearAction())
    }
}