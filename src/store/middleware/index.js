import { loginMiddleware } from "./loginMiddleware";
import { applyMiddleware } from "redux";
import { sessionMiddleware } from "./sessionMiddleware";
import { translationMiddleware } from "./translationMiddleware"; 

export default applyMiddleware(
    loginMiddleware,
    sessionMiddleware,
    translationMiddleware
)