export const ACTION_TRANSLATION_ATTEMPTING = '[translation] ATTEMPT'
export const ACTION_TRANSLATION_SUCCESS = '[translation] SUCCESS'
export const ACTION_TRANSLATION_ERROR = '[translation] ERROR'
export const ACTION_TRANSLATION_CLEAR = '[translation] CLEAR'

export const translationAttemptAction = translated => ({
    type: ACTION_TRANSLATION_ATTEMPTING,
    payload: translated
})

export const translationSuccessAction = translated => ({
    type: ACTION_TRANSLATION_SUCCESS,
    payload: translated
})

export const translationErrorAction = error => ({
    type: ACTION_TRANSLATION_ERROR,
    payload: error
})

export const translationClearAction = user => ({
    type: ACTION_TRANSLATION_CLEAR,
    payload: user
})

