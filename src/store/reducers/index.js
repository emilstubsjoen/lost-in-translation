import { combineReducers } from 'redux'
import { loginReducer } from './loginReducer'
import { sessionReducer } from './sessionReducer'
import { translationReducer } from './translationReducer'


const appReducer = combineReducers({
    login: loginReducer,
    session: sessionReducer,
    translation: translationReducer
})

export default appReducer