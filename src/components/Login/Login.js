import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import AppContainer from "../../hoc/AppContainer";
import { loginAttemptAction } from "../../store/actions/loginActions";

const initialState = {
  avatar: "",
  history: [],
  loggedIn: false,
};

const Login = () => {
  const dispatch = useDispatch();
  const { loginError, loginAttempting } = useSelector((state) => state.login);
  const { loggedIn } = useSelector((state) => state.session);

  const [credentials, setCredentials] = useState({
    username: "",
  });

  const onInputChange = (event) => {
    setCredentials({
      ...credentials,
      ...initialState,
      [event.target.id]: event.target.value,
    });
  };

  const onFormSubmit = (event) => {
    event.preventDefault(); // Prevent reload
    dispatch(loginAttemptAction(credentials));
  };
  return (
    <>
      {loggedIn && <Redirect to="/translation" />}
      {!loggedIn && (
        <AppContainer>
          <img src="images/Logo-Hello.png" className="mt-3" alt="" width="125" height="150" ></img>
          <form className="mt-3" onSubmit={onFormSubmit}>
            <h4>Get started</h4>
            <div className="textarea-container">
              <textarea
                id="username"
                type="text"
                className="form-control"
                placeholder="What's your name?"
                onChange={onInputChange}
                aria-label="What's your name?"
                aria-describedby="basic-addon2"
              />
              <button className="btn btn-default btn-lg mt-1" type="submit">
                <img src="images/start.png" alt="" width="40" height="40" />
              </button>
            </div>

            {loginAttempting && <p>Trying to login...</p>}

            {loginError && (
              <div className="alert aler-danger" role="alert">
                <h4>Unsuccessful</h4>
                <p className="mb-2">{loginError}</p>
              </div>
            )}
          </form>
        </AppContainer>
      )}
    </>
  );
};

export default Login;
