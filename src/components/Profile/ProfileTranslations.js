import { useSelector } from "react-redux"

const ProfileTranslations = () => {

    const { history } = useSelector(state => state.session)

    return (
        <>
            <h4 className="mb-3">Your last 10 tranlsations</h4>
                { history.map((translated, i) => {     
                    return (
                        <div className="card mt-5 mb-3">
                            <div>
                                {   translated.images.map((image, j) => {
                                    if (image !== " ") {
                                        return (
                                            <img key={i+j} src={image} alt="" width="30" height="30" />
                                        );
                                    } else {
                                        return <span>&nbsp;&nbsp;&nbsp;&nbsp;</span>;
                                    }
                            }) }
                            </div>
                        </div>     
                    )
                })}
                {/*history.map(post => <Post key={post.id} post={post} />)*/}
        </>

    )
}
export default ProfileTranslations