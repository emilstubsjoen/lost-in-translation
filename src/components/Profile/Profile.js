import { useDispatch, useSelector } from 'react-redux'
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import AppContainer from '../../hoc/AppContainer'
import { sessionLogoutAction } from '../../store/actions/sessionActions'
import { translationClearAction } from '../../store/actions/translationActions'
import ProfileTranslations from './ProfileTranslations'

export const Profile = () => {

    const mySwal = withReactContent(Swal)
    const dispatch = useDispatch()
    const { username = '' } = useSelector(state => state.session)
    const user = useSelector(state => state.session)

    const onLogoutClick = () => {
        mySwal.fire({
            backdrop: true,
            denyButtonText: 'Cancel',
            showCancelButton: true,
            confirmButtonColor: 'var(--bs-yellow)',
            cancelButtonColor: 'var(--bs-teal)',
            title: <p>Logout?</p>,
            text: 'Are you sure you want to logout?',
            cancelButtonText: 'No',
            confirmButtonText: 'Yes'
        }).then(result => {
            if (result.isConfirmed) {
                dispatch(sessionLogoutAction())
                window.location.reload(false)
            }
        })
    }

    const onClearClick = () => {
        mySwal.fire({
            backdrop: true,
            denyButtonText: 'Cancel',
            showCancelButton: true,
            confirmButtonColor: 'var(--bs-yellow)',
            cancelButtonColor: 'var(--bs-teal)',
            title: <p>Clear??</p>,
            text: 'Are you sure you want to clear your translation history?',
            cancelButtonText: 'No',
            confirmButtonText: 'Yes'
        }).then(result => {
            if (result.isConfirmed) {
                dispatch(translationClearAction(user))
            }
        })
    }

    return (
        <AppContainer>
            <img src="images/Logo.png" className="mt-3" alt="" width="125" height="150" ></img>
            <header className="mb-5">
                <h1>Hi, {username}</h1>
                <p>Welcome to your profile</p>
                <button className="btn btn-danger" onClick={onLogoutClick}>Logout</button>
            </header>

            <ProfileTranslations />
            <button className="btn btn-dark mb-3" onClick={onClearClick}>Clear</button>


        </AppContainer>
    )
}
export default Profile