import { useState } from "react";
import { translationAttemptAction } from "../../store/actions/translationActions";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import { useDispatch } from "react-redux";
import AppContainer from "../../hoc/AppContainer";
import { useSelector } from 'react-redux'
import { Redirect } from "react-router-dom";

const Translation = () => {
  const dispatch = useDispatch();
  const mySwal = withReactContent(Swal);
  const user = useSelector(state => state.session)
  const { username } = useSelector(state => state.session)

  const [sentence, setSentence] = useState({
    translationText: "",
    translationError: "",
  });

  const [images, setImages] = useState({
    images: [],
  });

  const onTranslateSentence = (e) => {
    e.preventDefault();

    if (!sentence.translationText) {
      mySwal
        .fire({
          backdrop: true,
          title: <p>Missed something</p>,
          html: '<span class="material-icons">error</span><br>You need to write a sentence to translate. 👌',
          confirmButtonText: "Gotcha",
        })
        .then((_) => {});
      return;
    }

    setImages({
      images: 
        images.images = sentence.translationText
        .slice()
        .split("")
        .map((letter) => {
          if (letter === " ") {
            return letter;
          } else {
            return `images/individial_signs/${letter}.png`;
          }
        })
      });
    dispatch(translationAttemptAction({
      user: user,
      translation: images}));
    
  };

  const onInputChange = (event) => {
    setSentence({
      ...sentence,
      [event.target.id]: event.target.value,
    });
    
  };

  return (
    <AppContainer>
      {!username && <Redirect to="/login" />}
      <img src="images/Logo.png" className="mt-3" alt="" width="125" height="150" ></img>
      <div className="textarea-container">
        <textarea
          id="translationText"
          type="text"
          className="form-control mt-3"
          placeholder="Write some text here"
          onChange={onInputChange}
          aria-label="Write some text here"
          aria-describedby="basic-addon2"
        />
        <button
          type="button"
          className="btn btn-default btn-lg mt-1"
          onClick={onTranslateSentence}
        >
          <img src="images/start.png" alt="" width="40" height="40" />
          {/*<span className="glyphicon glyphicon-align-left" aria-hidden="true"></span>*/}
        </button>
      </div>

      
      <div className="card mt-5">
        <div>
          {images.images.map((image, index) => {
            if (image !== " ") {
              return (
                <img key={index} src={image} alt="" width="30" height="30" />
              );
            } else {
              return <span>&nbsp;&nbsp;&nbsp;&nbsp;</span>;
            }
          })}
        </div>
      </div>
    </AppContainer>
  );
};

export default Translation;
