export const TranslationAPI = {
    async updateUser(user) {
        const requestConfig = {
          method: 'PATCH',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(user)
        }
      
        try {
          const updatedUser = await fetch(`http://localhost:3000/users/${user.id}`, requestConfig).then(r => r.json())
          return [ null, updatedUser ]
        }
        catch (error) {
              return [ error.message, null ]
        }
    },

    async updateUserTranslations(user) {
        
        user.user.history.push(user.translation);
        if (user.user.history.length > 10) user.user.history.pop()

        const [ error, updatedUser ] = await this.updateUser(user.user);
        return updatedUser
    },

    async clearUserTranslations(user) {
      user.history = [];
      const [ error, updatedUser ] = await this.updateUser(user);
      return updatedUser
  }


}